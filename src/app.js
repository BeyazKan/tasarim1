import _ from 'lodash';
import Holder from './js/holder.min.js';
import './app.scss';
import jBgImage from './images/ders.jpg';

Holder.addTheme('thumb', {
    bg: '#55595c',
    fg: '#eceeef',
    text: 'Thumbnail'
});